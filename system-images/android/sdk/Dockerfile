FROM ubuntu:18.04

# inspired by rabits/qt which we use for the gcc toolkit
MAINTAINER Aleix Pol <aleixpol@kde.org>

ARG QT_VERSION=5.15.0
ARG QT_TAG=v5.15.0
ARG NDK_VERSION=r21
ARG SDK_PLATFORM=android-21
ARG SDK_BUILD_TOOLS=29.0.3
ARG SDK_PACKAGES="tools platform-tools"

ENV DEBIAN_FRONTEND noninteractive
ENV QT_PATH /opt/Qt
ENV ANDROID_HOME /opt/android-sdk
ENV ANDROID_SDK_ROOT ${ANDROID_HOME}
ENV ANDROID_NDK_ROOT /opt/android-ndk
ENV ANDROID_NDK_HOST linux-x86_64
ENV ANDROID_NDK_PLATFORM ${SDK_PLATFORM}
ENV PATH ${QT_PATH}/bin:${ANDROID_HOME}/tools:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}/platform-tools:${PATH}

# Install updates & requirements:
#  * unzip - unpack platform tools
#  * git, openssh-client, ca-certificates - clone & build
#  * locales, sudo - useful to set utf-8 locale & sudo usage
#  * curl - to download Qt bundle
#  * make, openjdk-8-jdk, ant - basic build requirements
#  * libsm6, libice6, libxext6, libxrender1, libfontconfig1, libdbus-1-3 - dependencies of Qt bundle run-file
#  * libc6:i386, libncurses5:i386, libstdc++6:i386, libz1:i386 - dependencides of android sdk binaries
RUN dpkg --add-architecture i386 && apt update && apt full-upgrade -y && apt install -y --no-install-recommends \
    unzip \
    git \
    openssh-client \
    ca-certificates \
    locales \
    sudo \
    curl \
    make \
    openjdk-8-jdk \
    ant \
    build-essential \
    python3 \
    libsm6 \
    libice6 \
    libxext6 \
    libxrender1 \
    libfontconfig1 \
    libdbus-1-3 \
    libc6:i386 \
    libncurses5:i386 \
    libstdc++6:i386 \
    libz1:i386 \

    #build dependencies
    rdfind \
    python3-distutils \
    libxml-simple-perl \
    libjson-perl \
    libio-socket-ssl-perl \
    libyaml-perl libyaml-libyaml-perl \
    ninja-build \
    build-essential \
    gperf gettext \
    python3 python3-paramiko python3-lxml python3-yaml \
    bison flex \
    ruby wget \
    #for dbus/expat
    automake libtool autoconf autoconf-archive pkg-config \
    #for translations
    subversion gnupg2 \
    #for karchive
    zlib1g-dev \
    && apt-get -qq clean \
    && locale-gen en_US.UTF-8 && dpkg-reconfigure locales \
    && update-alternatives --install /usr/bin/python python /usr/bin/python3 10

##########################

RUN chmod a+w /opt/

# Add group & user
RUN groupadd -r user && useradd --create-home --gid user user && echo 'user ALL=NOPASSWD: ALL' > /etc/sudoers.d/user

RUN mkdir /output
RUN chown user:user /output

USER user
WORKDIR /home/user
ENV HOME /home/user

##########################

# Download & unpack android SDK
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64
RUN curl -Lo /tmp/sdk-tools.zip 'https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip' \
    && mkdir -p /opt/android-sdk && unzip -q /tmp/sdk-tools.zip -d /opt/android-sdk && rm -f /tmp/sdk-tools.zip \
    && yes | sdkmanager --licenses && sdkmanager --verbose "platforms;${SDK_PLATFORM}" "build-tools;${SDK_BUILD_TOOLS}" ${SDK_PACKAGES}

# Download & unpack android NDK
RUN mkdir /tmp/android && cd /tmp/android && curl -Lo ndk.zip "https://dl.google.com/android/repository/android-ndk-${NDK_VERSION}-linux-x86_64.zip" \
    && unzip -q ndk.zip && mv android-ndk-* $ANDROID_NDK_ROOT && chmod -R +rX $ANDROID_NDK_ROOT \
    && rm -rf /tmp/android \
    && rdfind /opt/android-ndk/ -makehardlinks true -makeresultsfile false

COPY build-openssl /opt/helpers/
RUN bash /opt/helpers/build-openssl

RUN export OPENSSL_LIBS='-L/opt/kdeandroid-arm/lib -lssl -lcrypto' && \
    cd && git clone git://code.qt.io/qt/qt5.git --single-branch --branch ${QT_TAG} && \
    cd qt5 && \
    ./init-repository && \
    ./configure -xplatform android-clang -openssl-runtime -nomake tests -nomake examples -android-ndk /opt/android-ndk -android-sdk /opt/android-sdk -android-abis armeabi-v7a,arm64-v8a -skip qtquick3d -skip qttranslations -skip qtserialport -skip qtwebengine -no-warnings-are-errors -opensource -confirm-license -prefix $QT_PATH -I/opt/kdeandroid-arm/include && \
    make -j`nproc` && \
    make -j`nproc` install && \
    cd .. && rm -rf qt5
##########################

RUN mkdir /opt/nativetooling

ENV ANDROID_NDK $ANDROID_NDK_ROOT
COPY gitconfig $HOME/.gitconfig

RUN mkdir -p /opt/cmake \
    && curl -Lo /tmp/cmake.sh https://cmake.org/files/v3.16/cmake-3.16.6-Linux-x86_64.sh \
    && bash /tmp/cmake.sh --skip-license --prefix=/opt/cmake --exclude-subdir \
    && rm /tmp/cmake.sh
ENV PATH="/opt/cmake/bin:${PATH}"

# makes sure gradle is downloaded, otherwise it will be downloaded every time
RUN $QT_PATH/src/3rdparty/gradle/gradlew

# compile kf5 tooling
RUN cd && git clone git://code.qt.io/qt/qtbase.git --single-branch --branch ${QT_TAG} && cd qtbase \
    && ./configure -prefix /opt/nativetooling -opensource -confirm-license -no-gui -release -optimize-size -nomake tests -nomake examples -no-feature-concurrent \
    && make -j`nproc` && make install && rm -rf ~/qtbase


# QtQml Needed for native tooling (ki18n)
RUN cd && git clone git://code.qt.io/qt/qtdeclarative.git --single-branch --branch ${QT_TAG} && cd qtdeclarative \
    && QMAKESPEC=linux-g++ /opt/nativetooling/bin/qmake . && make -j`nproc` && make install && rm -rf ~/qtdeclarative

COPY build-cmake-native git-clone-macro.sh /opt/helpers/
ENV PERSIST=0
RUN /opt/helpers/build-cmake-native extra-cmake-modules kde:extra-cmake-modules -DCMAKE_INSTALL_PREFIX=/opt/nativetooling -DCMAKE_PREFIX_PATH="$QT_PATH;/opt/nativetooling" -DANDROID_ABI=arm64-v8a
RUN /opt/helpers/build-cmake-native kconfig     kde:kconfig     -DCMAKE_INSTALL_PREFIX=/opt/nativetooling -DBUILD_SHARED_LIBS=OFF -DBUILD_TESTING=OFF -DCMAKE_PREFIX_PATH=/opt/nativetooling -DKCONFIG_USE_GUI=OFF
RUN /opt/helpers/build-cmake-native kcoreaddons kde:kcoreaddons -DCMAKE_INSTALL_PREFIX=/opt/nativetooling -DBUILD_SHARED_LIBS=OFF -DBUILD_TESTING=OFF -DCMAKE_PREFIX_PATH=/opt/nativetooling -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Widgets=ON

COPY build-cmake /opt/helpers/
ENV STANDALONE_EXTRA="--stl=libc++"

#build expat
RUN EXTRA_CMAKE_SUBDIR=expat /opt/helpers/build-cmake libexpat https://github.com/libexpat/libexpat.git

#build dbus
RUN EXTRA_CMAKE_SUBDIR=cmake GIT_EXTRA="--branch dbus-1.12.16" /opt/helpers/build-cmake dbus git://anongit.freedesktop.org/git/dbus/dbus

#build qtdbus
RUN    sed -i.bak "s|#define QT_NO_DBUS|//#define QT_NO_DBUS //we install it separate from the SDK|g" $QT_PATH/include/QtCore/qconfig.h \
    && sed -i.bak "s/QT_FEATURE_dbus -1/QT_FEATURE_dbus 1/g" $QT_PATH/include/QtCore/${QT_VERSION}/QtCore/private/qconfig_p.h \
    && sed -i.bak "s/QT_FEATURE_dbus_linked -1/QT_FEATURE_dbus_linked 1/g" $QT_PATH/include/QtCore/${QT_VERSION}/QtCore/private/qconfig_p.h \
    && cd && git clone git://code.qt.io/qt/qtbase.git --single-branch --branch ${QT_TAG} && cd qtbase/src/dbus && qmake && make -j`nproc` && make install && cd && rm -rf qtbase \
    && ln -s /opt/nativetooling/bin/qdbusxml2cpp $QT_PATH/bin/ \
    && ln -s /opt/nativetooling/bin/qdbuscpp2xml $QT_PATH/bin/

# needs to be after building qt, otherwise it breaks weirdly
ENV QMAKESPEC android-clang

RUN /opt/helpers/build-cmake libintl-lite https://github.com/j-jorge/libintl-lite.git
COPY build-autotools build-qt build-generic build-eigen build-poppler build-vlcqt build-gstreamer-binaries build-kde-dependencies build-kde-project create-apk get-apk-args.py \
     skip build-standalone build-discount 0001-Hack-Use-mktags-from-native-build-when-crosscompilin.patch /opt/helpers/

RUN     sed -i.bak 's/android:minSdkVersion="16"/android:minSdkVersion="21"/g' $QT_PATH/src/android/templates/AndroidManifest.xml \
    &&  sed -i.bak 's/|density//g' $QT_PATH/src/android/templates/AndroidManifest.xml

ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8
ENV PERSIST=1
